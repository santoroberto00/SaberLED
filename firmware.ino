#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma once

int free() {
	extern int __heap_start, *__brkval;
	int v;
	return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

#define SERIAL_DEBUG true
#include <FormattingSerialDebug.h>

#include <CapacitiveSensor.h>
CapacitiveSensor btn = CapacitiveSensor(4, 5);

#include <SparkFun_ADXL345.h>
ADXL345 accel = ADXL345();

#define BATTERY_PIN A0
#include <Battery.h>
Battery battery = Battery(7400, 8400, BATTERY_PIN);

#define SD_PIN 10
#define SPEAKER_PIN 9
#include <SPI.h>
#include <SdFat.h>
#include <TMRpcm.h>
SdFat sd;
TMRpcm pcm;

static const char boot_wav[] PROGMEM = "boot.wav";
static const char on_wav[] PROGMEM   = "on.wav";
static const char hum_wav[] PROGMEM  = "hum.wav";
static const char off_wav[] PROGMEM  = "off.wav";

#define SWING_COUNT 4
static const char swing_1[] PROGMEM = "swing1.wav";
static const char swing_2[] PROGMEM = "swing2.wav";
static const char swing_3[] PROGMEM = "swing3.wav";
static const char swing_4[] PROGMEM = "swing4.wav";
const char* const swing_wav[] PROGMEM = { swing_1, swing_2, swing_3, swing_4 };

#define HIT_COUNT 4
static const char hit_1[] PROGMEM = "hit1.wav";
static const char hit_2[] PROGMEM = "hit2.wav";
static const char hit_3[] PROGMEM = "hit3.wav";
static const char hit_4[] PROGMEM = "hit4.wav";
const char* const hit_wav[] PROGMEM = { hit_1, hit_2, hit_3, hit_4 };
char filename[10];

#define NUM_LEDS 			288
#define DATA_PIN 			6
#define HIT_HUE_VARIATION 	20
#define HIT_HUE_LATENCY 	5
#define SABER_SPEED 		128
#define SABER_BRIGHTNESS 	128
#define BITS_PER_PIXEL		4
#define NUM_COLORS			(1<<BITS_PER_PIXEL)
#include "RGBColor.h"
#include "LED.h"
WS2812B<D, 6> strip;
grb palette[NUM_COLORS] = { };
uint8_t pixels[ARRAY_SIZE(NUM_LEDS, BITS_PER_PIXEL)];

#define DEBOUNCE 15
#define DMASK ((1ULL<<DEBOUNCE)-1)
#define DF (1ULL<<(DEBOUNCE-1))
#define DR (DMASK-DF)
// macro for detection of raising edge and debouncing
#define DRE(signal, state) ((state=((state<<1)|(signal&1))&DMASK)==DR)

#include "Elapsed.h"

#define OFF				0
#define ON				1
#define SWING			2
#define HIT				3
#define BATTERY_PROTECT	9
uint8_t status = OFF;

void setup() {
	SERIAL_DEBUG_SETUP(9600);
	DEBUG(F("BOOTING UP (%i)"), free());
	btn.set_CS_AutocaL_Millis(60000);

	battery.begin();

	strip.clear(NUM_LEDS);
	// set saber color
	setupPalette(0); // map(analogRead(CRYSTAL_PIN),255);
	setupAudio();
	setupAccel();

	DEBUG(F("BOOT COMPLETED(%i)"), free());
}

void loop() {
	checkButton();
	// temporarily disabled
	// checkBattery();
	if (status != OFF || status != BATTERY_PROTECT) {
		if (!pcm.isPlaying()) {
			strcpy_P(filename, hum_wav);
			pcm.play(filename);
		}
		uint8_t source = accel.getInterruptSource();	//accel.readRegister(ADXL345_REG_INT_SOURCE);
		if ((source >> 6) & 0x01) {
			status = HIT;
			hit();
			status = ON;
		} else if (((source >> 4) & 0x01) && status == ON) {
			status = SWING;
			swing();
			status = ON;
		}
	}
}

Elapsed btnTouched;
uint16_t btnState;
void inline __attribute__ ((always_inline)) checkButton() {
	if (btnTouched > 500) {
		bool touch = btn.capacitiveSensor(30) > 200;
		if (DRE(touch, btnState)) {
			DEBUG(F("button pressed, state is %i"), status);
			btnTouched = 0;
			if (status == OFF) {
				on();
				status = ON;
			} else if (status == ON) {
				status = OFF;
				off();
			}
		}
	}
}

void inline __attribute__ ((always_inline)) on() {
	DEBUG(F("TURNING ON"));
	strcpy_P(filename, on_wav);
	pcm.play(filename);
	for (uint16_t i = 0; i < NUM_LEDS / 2; i++) {
		SET_PIXEL(pixels, i, BITS_PER_PIXEL, 2);
		SET_PIXEL(pixels, NUM_LEDS - 1 - i, BITS_PER_PIXEL, 2);
		if (i % 20 == 0) {
			strip.sendPixels<BITS_PER_PIXEL, grb>(NUM_LEDS, pixels, palette);
		}
	}
	strip.sendPixels<BITS_PER_PIXEL, grb>(NUM_LEDS, pixels, palette);
	DEBUG(F("SABER ON(%i)"), free());
}

void inline __attribute__ ((always_inline)) off() {
	DEBUG(F("TURNING OFF"));
	strcpy_P(filename, off_wav);
	pcm.play(filename);
	for (uint16_t i = 0; i < NUM_LEDS / 2; i++) {
		SET_PIXEL(pixels, NUM_LEDS / 2 + i, BITS_PER_PIXEL, 0);
		SET_PIXEL(pixels, NUM_LEDS / 2 - i - 1, BITS_PER_PIXEL, 0);
		if (i % 8 == 0) {
			strip.sendPixels<BITS_PER_PIXEL, grb>(NUM_LEDS, pixels, palette);
		}
	}
	strip.sendPixels<BITS_PER_PIXEL, grb>(NUM_LEDS, pixels, palette);
	DEBUG(F("SABER OFF(%i)"), free());
}

void inline __attribute__ ((always_inline)) swing() {
	uint8_t index = rand() % SWING_COUNT;
	DEBUG(F("SWING...[%i] (%i)"), index, free());
	strcpy_P(filename, (char*) pgm_read_word(&(swing_wav[index])));
	pcm.play(filename);
}

void inline __attribute__ ((always_inline)) hit() {
	uint8_t index = rand() % HIT_COUNT;
	DEBUG(F("HIT [%i] (%i)"), index, free());
	strcpy_P(filename, (char*) pgm_read_word(&(hit_wav[index])));
	pcm.play(filename);
	for (uint8_t i = 0; i < 3; i++) {
		blade(4 + i);
		delay(HIT_HUE_LATENCY);
		blade(9 - i);
		delay(HIT_HUE_LATENCY);
	}
	blade(2);
}

void inline __attribute__ ((always_inline)) blade(uint8_t color) {
	for (uint16_t i = 0; i < NUM_LEDS; i++) {
		SET_PIXEL(pixels, i, BITS_PER_PIXEL, color);
	}
	strip.sendPixels<BITS_PER_PIXEL, grb>(NUM_LEDS, pixels, palette);
}

Elapsed batteryCheck;
void inline __attribute__ ((always_inline)) checkBattery() {
	if (batteryCheck > 30000) {
		DEBUG(F("battery voltage is %imV (%i%%)"), battery.voltage(), battery.level());
		batteryCheck = 0;
		uint8_t blinks = 0;
		if (battery.level() < 20) {
			blinks = 5;
		} else if (battery.level() <= 10) {
			blinks = 10;
		} else if (battery.level() < 3) {
			// battery is dying: let's protect it from unrecoverable discharging
			off();
			status = BATTERY_PROTECT;
		}
		for (uint8_t i = 0; i < blinks; i++) {
			blade(0);
			delay(100);
			blade(1);
			delay(100);
		}
	}
}

void setupPalette(uint8_t hue) {
	RGBColor color;
	// set strip palette
	color.fromHSV(hue, 0, 0);
	palette[0] = {color.green, color.red, color.blue};
	color.fromHSV(hue, 255, SABER_BRIGHTNESS / 2);
	palette[1] = {color.green, color.red, color.blue};
	color.fromHSV(hue, 255, SABER_BRIGHTNESS);
	palette[2] = {color.green, color.red, color.blue};

	color.fromHSV(hue - (HIT_HUE_VARIATION / 4), 255, SABER_BRIGHTNESS);
	palette[4] = {color.green, color.red, color.blue};
	color.fromHSV(hue - (HIT_HUE_VARIATION / 2), 255, SABER_BRIGHTNESS);
	palette[5] = {color.green, color.red, color.blue};
	color.fromHSV(hue - (HIT_HUE_VARIATION), 255, SABER_BRIGHTNESS);
	palette[6] = {color.green, color.red, color.blue};
	color.fromHSV(hue + (HIT_HUE_VARIATION), 255, SABER_BRIGHTNESS);
	palette[7] = {color.green, color.red, color.blue};
	color.fromHSV(hue + (HIT_HUE_VARIATION / 2), 255, SABER_BRIGHTNESS);
	palette[8] = {color.green, color.red, color.blue};
	color.fromHSV(hue + (HIT_HUE_VARIATION / 4), 255, SABER_BRIGHTNESS);
	palette[9] = {color.green, color.red, color.blue};

	color.fromHSV(hue + 128, 255, SABER_BRIGHTNESS);
	palette[15] = {color.red, color.green, color.blue};
}

void setupAccel() {
	accel.powerOn();
	if (accel.status != ADXL345_OK) {
		DEBUG(F("No ADXL345 detected"));
	} else {
		// setup
		accel.setRangeSetting(8);
		accel.setSpiBit(1);

		accel.setActivityXYZ(1, 1, 1);
		accel.setActivityThreshold(0x40);
		accel.setInactivityXYZ(0, 0, 0);
		accel.setInactivityThreshold(0x40);
		accel.setTimeInactivity(10);

		accel.setTapDetectionOnXYZ(1, 1, 1);
		accel.setTapThreshold(0x60);
		accel.setTapDuration(0x10);
		accel.setDoubleTapLatency(0xFF);
		accel.setDoubleTapWindow(0);
		accel.setFreeFallThreshold(0);
		accel.setFreeFallDuration(0);

		// enable interrupts for ACT and TAP
		accel.ActivityINT(1);
		accel.singleTapINT(1);
	}
}

void setupAudio() {
	pcm.speakerPin = SPEAKER_PIN;
	//Complimentary Output or Dual Speakers:
	//pinMode(10,OUTPUT);

	if (!sd.begin(SD_PIN, SPI_FULL_SPEED)) {
		DEBUG(F("No SD detected"));
	} else {
		strcpy_P(filename, boot_wav);
		pcm.play(filename);
	}
}
