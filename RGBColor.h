#pragma once

#define HSV_SECTION_6 (0x20)
#define HSV_SECTION_3 (0x40)
struct RGBColor {
	uint8_t red;
	uint8_t green;
	uint8_t blue;

	void fromHSV(uint8_t hue, uint8_t saturation, uint8_t value) {

		// Saturation more useful the other way around
		saturation = 255 - saturation;

		// The brightness floor is minimum number that all of
		// R, G, and B will be set to, which is value * invsat
		uint8_t brightness_floor;

		asm volatile(
				"mul %[value], %[saturation]        \n"
				"mov %[brightness_floor], r1        \n"
				: [brightness_floor] "=r" (brightness_floor)
				: [value] "r" (value),
				[saturation] "r" (saturation)
				: "r0", "r1"
		);

		// The color amplitude is the maximum amount of R, G, and B
		// that will be added on top of the brightness_floor to
		// create the specific hue desired.
		uint8_t color_amplitude = value - brightness_floor;

		// Figure how far we are offset into the section of the
		// color wheel that we're in
		uint8_t offset = hue & (HSV_SECTION_3 - 1);  // 0..63
		uint8_t rampup = offset * 4; // 0..252

		// compute color-amplitude-scaled-down versions of rampup and rampdown
		uint8_t rampup_amp_adj;
		uint8_t rampdown_amp_adj;

		asm volatile(
				"mul %[rampup], %[color_amplitude]       \n"
				"mov %[rampup_amp_adj], r1               \n"
				"com %[rampup]                           \n"
				"mul %[rampup], %[color_amplitude]       \n"
				"mov %[rampdown_amp_adj], r1             \n"
				: [rampup_amp_adj] "=&r" (rampup_amp_adj),
				[rampdown_amp_adj] "=&r" (rampdown_amp_adj),
				[rampup] "+r" (rampup)
				: [color_amplitude] "r" (color_amplitude)
				: "r0", "r1"
		);

		// add brightness_floor offset to everything
		uint8_t rampup_adj_with_floor = rampup_amp_adj + brightness_floor;
		uint8_t rampdown_adj_with_floor = rampdown_amp_adj + brightness_floor;

		// keep gcc from using "X" as the index register for storing
		// results back in the return structure.  AVR's X register can't
		// do "std X+q, rnn", but the Y and Z registers can.
		// if the pointer to 'rgb' is in X, gcc will add all kinds of crazy
		// extra instructions.  Simply killing X here seems to help it
		// try Y or Z first.
		asm volatile( "" : : : "r26", "r27" );

		if (hue & 0x80) {
			// section 2: 0x80..0xBF
			red = rampup_adj_with_floor;
			green = brightness_floor;
			blue = rampdown_adj_with_floor;
		} else {
			if (hue & 0x40) {
				// section 1: 0x40..0x7F
				red = brightness_floor;
				green = rampdown_adj_with_floor;
				blue = rampup_adj_with_floor;
			} else {
				// section 0: 0x00..0x3F
				red = rampdown_adj_with_floor;
				green = rampup_adj_with_floor;
				blue = brightness_floor;
			}
		}

		asm volatile( "clr __zero_reg__  \n\t" : : : "r1" );
	}
};
